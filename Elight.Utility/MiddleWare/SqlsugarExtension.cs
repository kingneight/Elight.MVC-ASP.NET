﻿#if !NETFRAMEWORK
using Elight.Utility.Extension;
using Elight.Utility.Extension.DataCache;
using Elight.Utility.Extension.SqlSugar;
using JinianNet.JNTemplate;
using Microsoft.Extensions.DependencyInjection;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Elight.Utility.MiddleWare
{
    public static class SqlsugarExtension
    {
        public static void AddSqlsugarServer(this IServiceCollection services, string type, string host, string dbName, string userName, string pasword, bool debug = false)
        {
            DbType dbType;
            string connectionString = "";
            var slavaConFig = new List<SlaveConnectionConfig>();
            switch (type.ToUpper())
            {
                case "POSTGRESQL": dbType = DbType.PostgreSQL; connectionString = $"Server={host};Port=5432;Database={dbName};User Id={userName};Password={pasword};"; break;
                case "MYSQL": dbType = DbType.MySql; connectionString = $"server={host};Database={dbName};Uid={userName};Pwd={pasword};"; break;
                case "SQLITE":
                    dbType = DbType.Sqlite;
                    var template = Engine.CreateTemplate(dbName);
                    template.Set("BaseDirectory", MyEnvironment.RootPath(""));
                    var result = template.Render();
                    connectionString = $"DataSource={result};"; ; break;
                case "SQLSERVER": dbType = DbType.SqlServer; connectionString = $"Data Source={host};Initial Catalog={dbName};User ID={userName};Password={pasword};"; break;
                case "MSSQL": dbType = DbType.SqlServer; connectionString = $"Data Source={host};Initial Catalog={dbName};User ID={userName};Password={pasword};"; break;
                case "ORACLE": dbType = DbType.Oracle; connectionString = $"Data Source={host}/{dbName};User ID={userName};Password={pasword};"; break;
                default: throw new Exception("配置写的TM是个什么东西？");
            }

            SqlSugarScope sqlSugar = new SqlSugarScope(
                new ConnectionConfig()
                {
                    //准备添加分表分库
                    DbType = dbType,
                    ConnectionString = connectionString,
                    IsAutoCloseConnection = true,
                    MoreSettings = new ConnMoreSettings()
                    {
                        DisableNvarchar = true
                    },
                    SlaveConnectionConfigs = slavaConFig,
                    //设置codefirst非空值判断
                    ConfigureExternalServices = new ConfigureExternalServices
                    {
                        SqlFuncServices = ExtMethods.GetExpMethods,
                        DataInfoCacheService = new HttpRuntimeCache(),
                        EntityService = (c, p) =>
                        {
                            //高版C#写法 支持string?和string  
                            if (new NullabilityInfoContext()
                            .Create(c).WriteState is NullabilityState.Nullable)
                            {
                                p.IsNullable = true;
                            }
                        }
                    }
                },
                db =>
                {
                    db.Aop.DataExecuting = (oldValue, entityInfo) =>
                    {
                        //var httpcontext = ServiceLocator.Instance.GetService<IHttpContextAccessor>().HttpContext;
                        switch (entityInfo.OperationType)
                        {
                            case DataFilterType.InsertByObject:
                                if (entityInfo.PropertyName == "CreateUser")
                                {
                                    //entityInfo.SetValue(new Guid(httpcontext.Request.Headers["Id"].ToString()));
                                }
                                if (entityInfo.PropertyName == "TenantId")
                                {
                                    //entityInfo.SetValue(new Guid(httpcontext.Request.Headers["TenantId"].ToString()));
                                }
                                break;
                            case DataFilterType.UpdateByObject:
                                if (entityInfo.PropertyName == "ModifyTime")
                                {
                                    entityInfo.SetValue(DateTime.Now);
                                }
                                if (entityInfo.PropertyName == "ModifyUser")
                                {
                                    //entityInfo.SetValue(new Guid(httpcontext.Request.Headers["Id"].ToString()));
                                }
                                break;
                        }

                    };
                    db.Aop.OnLogExecuting = (sql, pars) =>
                    {
                        if (debug)
                        {
                            Console.WriteLine(sql + "\r\n" + db.Utilities.SerializeObject(pars.ToDictionary(it => it.ParameterName, it => it.Value)));
                            Console.WriteLine();
                        }
                    };
                }
            );
            services.AddSingleton<ISqlSugarClient>(sqlSugar);//这边是SqlSugarScope用AddSingleton
        }
    }
}
#endif