﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elight.Entity.GlobalData
{
    /// <summary>
    /// 配置文件
    /// </summary>
    public class ServerConfigs
    {

        /// <summary>
        /// 数据库类型
        /// </summary>
        public string DbType { get; set; }
        /// <summary>
        /// 数据库主机IP
        /// </summary>
        public string DbHost { get; set; }

        /// <summary>
        /// 数据库名
        /// </summary>
        public string DbName { get; set; }

        /// <summary>
        /// 数据库用户名
        /// </summary>
        public string DbUserName { get; set; }

        /// <summary>
        /// 数据库密码
        /// </summary>
        public string DbPassword { get; set; }

        /// <summary>
        /// 网页端端口号
        /// </summary>
        public int HttpPort { get; set; }

        /// <summary>
        /// 是否使用IIS部署
        /// </summary>
        public bool IISDeploy { get; set; }
        /// <summary>
        /// 网页端软件名
        /// </summary>
        [Config("软件名")]
        public string WebSoftwareName { get; set; }

        /// <summary>
        /// 版权信息
        /// </summary>
        [Config("版权信息")]
        public string Copyright { get; set; }

        /// <summary>
        /// 日志文件清理周期
        /// </summary>
        [Config("日志清理周期")]
        public int LogOutDateDays { get; set; }


        public int SessionTimeout { get; set; }

        private bool _debug = false;
        /// <summary>
        /// 是否调试模式，调试模式将会输出日志
        /// </summary>
        public bool Debug { get { return _debug; } set { _debug = value; } }

        [Config("代码路径")]
        public string CodeDir { get; set; }
    }
}
