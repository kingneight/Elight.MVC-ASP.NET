﻿using Elight.Entity;
using Elight.Reposity;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Elight.Service
{
    public interface IBaseService<T, TRepository> where T : BaseModelEntity, new()
    {
        IRepository<T> _repository { get; set; }
        bool Delete(List<string> primaryKey);
        T Get(string primaryKey);
        List<T> GetList(int pageIndex, int pageSize, Expression<Func<T, bool>> expression, ref int totalCount);
    }
}
